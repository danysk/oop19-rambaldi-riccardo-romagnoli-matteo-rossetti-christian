package model.world;

public final class WorldSettings {

    /**
     * Height of the {@link GameWorld}.
     */
    public static final double WORLD_HEIGHT = 30;

    /**
     * Width of the {@link GameWorld}.
     */
    public static final double WORLD_WIDTH = 30;

    private WorldSettings() {
    }

}
