package view.menu;

/**
 * This interface allows to setup and show the main menu.
 */
public interface MainMenuView {

    /**
     * Shows the menu.
     */
    void showMenu();
}
